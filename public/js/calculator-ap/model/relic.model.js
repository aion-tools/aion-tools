export class RelicModel {
    /** @type {string} */
    slug;
    /** @type {string} */
    type;
    /** @type {number} */
    quantity;
    /** @type {RateModel} */
    rate;
}