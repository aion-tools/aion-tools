export class MemberModel {
    /** @type {string} */
    pseudo
    /** @type {string[]} */
    relics = [];
    /** @type {string[]} */
    relicsTemp = [];

    /**
     * @param {string} pseudo
     */
    constructor(pseudo) {
        this.pseudo = pseudo;
    }
}