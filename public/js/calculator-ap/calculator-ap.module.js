import {DistributionService} from "./service/distribution.service.js";
import {MembersService} from "./service/members.service.js";
import {RelicsService} from "./service/relics.service.js";

export class CalculatorApModule {
    distributionService;
    membersService;
    relicsService;

    constructor() {
        this.distributionService = new DistributionService();
        this.membersService = new MembersService(this.distributionService);
        this.relicsService = new RelicsService(this.distributionService, this.membersService);
        this.distributionService.membersService = this.membersService;
        this.distributionService.relicsService = this.relicsService;
    }
}