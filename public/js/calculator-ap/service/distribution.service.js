import {MemberModel} from "../model/member.model.js";

export class DistributionService {
    /** @type {RelicModel[]} */
    #relics= [];
    /** @type {MemberModel[]} */
    #members = [];
    /** @type {Map<string, number>} */
    #relicsToDistribute = new Map();
    /** @type {HTMLSpanElement} */
    totalAP = document.getElementById('total-ap');
    /** @type {HTMLButtonElement} */
    buttonDistribution = document.querySelector('button[data-bs-target="#distributionModal"]');
    /** @type {HTMLDivElement} */
    modalBody = document.querySelector("#distributionModal .modal-body");
    /** @type {MembersService} */
    membersService;
    /** @type {RelicsService} */
    relicsService;
    translate;
    parser = new DOMParser();

    constructor() {
        this.buttonDistribution.onclick = () => this.distribution();
        this.hasDistribution();
        document
            .querySelectorAll("#distributionModal button[name='validate']")
            .forEach(button => button.onclick = () => this.validateDistribution(true))
        ;
        document
            .querySelectorAll("#distributionModal button[name='close']")
            .forEach(button => button.onclick = () => this.validateDistribution(false))
        ;
        this.findTranslate().then();
    }

    async findTranslate() {
        let response = await fetch('/static/js/calculator-ap/translate.json');
        this.translate = await response.json();
    }

    /**
     * @param {string} id
     * @return {string}
     */
    trans(id) {
        return this.translate[id];
    }

    /**
     * @returns {RelicModel[]}
     */
    get relics() {
        return this.#relics;
    }

    /**
     * @param {RelicModel[]} relics
     */
    set relics(relics) {
        this.#relics = relics;
        this.calculTotalAP();
    }

    /**
     * @returns {MemberModel[]}
     */
    get members() {
        return this.#members;
    }

    /**
     * @param {MemberModel} member
     */
    set member(member) {
        this.#members.push(member);
        this.hasDistribution();
    }

    /**
     * @param {string} slug
     * @param {number} quantity
     * @return {number}
     */
    plusRelic(slug, quantity) {
        if (this.#relicsToDistribute.has(slug)) {
            quantity = this.#relicsToDistribute.get(slug)
        }

        this.#relicsToDistribute.set(slug, ++quantity);
        this.calculTotalAP();

        return this.#relicsToDistribute.get(slug);
    }

    /**
     * @param {string} slug
     * @param {number} quantity
     * @return {number}
     */
    dashRelic(slug, quantity) {
        if (this.#relicsToDistribute.has(slug)) {
            quantity = this.#relicsToDistribute.get(slug);
        }

        quantity -= 1;

        this.#relicsToDistribute.set(slug, quantity >= 0 ? quantity : 0);
        this.calculTotalAP();

        return this.#relicsToDistribute.get(slug);
    }

    calculTotalAP() {
        let totalAP = 0;

        this
            .#relicsToDistribute
            .forEach((quantity, slug) => {
                totalAP += this.pointsByRelic(slug) * quantity;
            })
        ;

        this.totalAP.textContent =new Intl.NumberFormat('fr-FR').format(totalAP);
        this.hasDistribution();
    }

    hasDistribution() {
        this.buttonDistribution.disabled = !(Number(this.totalAP.textContent.replaceAll(/\s/g, '')) > 0 && this.#members.length > 0);
    }

    distribution() {
        Array
            .from(this.#relicsToDistribute, ([slug, quantity]) => ({ slug, quantity }))
            .sort((a, b) => b.quantity - a.quantity)
            .forEach(relic => {
                while (relic.quantity-- > 0) {
                    const memberLessPoints = this.
                        #members
                        .reduce((prevMember, currentMember) => {
                            const pointsPrev = this.pointsByRelic(...prevMember.relics, ...prevMember.relicsTemp),
                                pointsCurrent = this.pointsByRelic(...currentMember.relics, ...currentMember.relicsTemp);

                            return pointsPrev < pointsCurrent ? prevMember : currentMember;
                        })
                    ;

                    memberLessPoints.relicsTemp.push(relic.slug);
                }
            })
        ;

        this.renderView();
    }

    /**
     * @param {string} slugs
     * @return {number}
     */
    pointsByRelic(...slugs) {
        return slugs.reduce((acc, slug) => {
            let filter = this.relics.filter(relic => relic.slug === slug);

            return acc + (filter.length > 0 ? filter[0].quantity : 0);
        }, 0);
    }

    renderView() {
        Array
            .from(this.modalBody.children)
            .forEach(child => child.remove())
        ;

        this
            .#members
            .forEach(member => {
                let html = `
                    <ul class="list-group col-12 col-md-6 col-lg-4">
                        <li class="list-group-item list-group-item-warning fw-bolder">${member.pseudo}</li>
                    </ul>
                `;

                let memberView = this
                    .parser
                    .parseFromString(html, 'text/html')
                    .body
                    .firstElementChild
                ;

                const relics = {};

                member.relicsTemp.forEach(value => {
                    if (relics[value]) {
                        relics[value]++;
                    } else {
                        relics[value] = 1;
                    }
                });

                /** @type {[string, number][]}*/
                let entries = Object.entries(relics);

                entries.forEach(([slug, quantity]) => {
                        let html = `
                            <li class="list-group-item">
                                <img src="/static/images/relics/${slug}.png" alt="${this.trans(slug)}" height="24" width="24"/>
                                <span class="fw-bolder">${this.trans(slug)} :</span>
                                ${new Intl.NumberFormat('fr-FR').format(quantity)}
                            </li>
                        `;

                        let relicView = this
                            .parser
                            .parseFromString(html, 'text/html')
                            .body
                            .firstElementChild
                        ;

                        memberView.append(relicView);
                    })
                ;

                const pointsCurrent = entries
                    .map(([slug, quantity]) => this.pointsByRelic(slug) * quantity)
                    .reduce((a, b) => a + b, 0)
                ;
                let htmlTotal = `
                    <li class="list-group-item list-group-item-warning">
                        <span class="fw-bolder">Total :</span>
                        ${new Intl.NumberFormat('fr-FR').format(pointsCurrent)}
                    </li>
                `;

                let relicView = this
                    .parser
                    .parseFromString(htmlTotal, 'text/html')
                    .body
                    .firstElementChild
                ;

                memberView.append(relicView);

                this.modalBody.append(memberView);
            })
        ;
    }

    validateDistribution(isValidate) {
        if (!isValidate) {
            this.#members.forEach(member => member.relicsTemp = []);
            return;
        }

        this
            .#members
            .forEach(member => {
                member.relics.push(...member.relicsTemp);
                member.relicsTemp = [];
            })
        ;

        this.#relicsToDistribute.clear();

        this.membersService.renderView();
        this.relicsService.reset();
    }
}