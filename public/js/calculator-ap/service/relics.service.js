import {MembersService} from "./members.service.js";

export class RelicsService {
    /** @type {HTMLSelectElement} */
    selectRate = document.getElementById('rate-relics');
    /** @type {HTMLDivElement} */
    listRelics = document.getElementById('relics');
    /** @type {DistributionService} */
    distributionService;
    /** @type {MembersService} */
    membersService;
    parser = new DOMParser();

    constructor(distributionService, membersService) {
        this.distributionService = distributionService;
        this.membersService = membersService;
        this.findRates().then();
    }

    async findRates() {
        let response = await fetch('/api/rates');

        if (!response.ok) {
            return;
        }

        /** @type {RateModel[]} */
        let json = await response.json();

        json
            .forEach(rate => {
                let option = document.createElement('option');
                option.value = rate.slug;
                option.textContent = rate.slug.charAt(0).toUpperCase() + rate.slug.slice(1);

                this.selectRate.add(option)
            })
        ;

        this.selectRate.onchange = () => this.changeSelectRate();
        await this.changeSelectRate();
    }

    async changeSelectRate() {
        let response = await fetch(`/api/relics/${this.selectRate.value}`);

        if (!response.ok) {
            return;
        }

        this.distributionService.relics = await response.json();
        this.renderView();
        this.membersService.renderView();
    }

    renderView() {
        if (this.listRelics.childElementCount > 0) {
            this
                .distributionService
                .relics
                .forEach(relic => document.getElementById(relic.slug).textContent = relic.quantity.toString())
            ;
            return;
        }

        [...new Set(this.distributionService.relics.map(relic => relic.type))]
            .forEach(type => {
                let html = `
                    <div class="col-12 col-md-6 p-xxl-1">
                        <h3 class="mt-2">${this.distributionService.trans(type)}</h3>
                    </div>
                `;

                let typeHtml = this
                    .parser
                    .parseFromString(html, 'text/html')
                    .body
                    .firstElementChild
                ;

                this
                    .distributionService
                    .relics
                    .filter(relic => relic.type === type)
                    .forEach(relic => {
                        let html = `
                            <div class="input-group bg-white">
                                <span class="input-group-text bg-white p-1">
                                    <img alt="" height="24" width="24" src="/static/images/relics/${relic.slug}.png">
                                </span>
                                <span class="input-group-text bg-white col p-1 fw-light d-flex flex-column align-items-start">
                                    <span class="fw-bolder">${this.distributionService.trans(relic.slug)}</span>
                                    <small class="fst-italic">(<span id="${relic.slug}">${new Intl.NumberFormat('fr-FR').format(relic.quantity)}</span> AP)</small>
                                </span>
                                <span class="input-group-text bg-white number-relics">0</span>
                                <button class="btn btn-outline-secondary" name="plus">
                                    <img alt="" height="16" width="16" src="/static/images/svg/plus.svg">
                                </button>
                                <button class="btn btn-outline-secondary" disabled="" name="dash">
                                    <img alt="" height="16" width="16" src="/static/images/svg/dash.svg">
                                </button>
                            </div>
                        `;

                        let relicHtml = this
                            .parser
                            .parseFromString(html, 'text/html')
                            .body
                            .firstElementChild
                        ;

                        let buttonPlus = relicHtml.querySelector('button[name="plus"]'),
                            buttonDash = relicHtml.querySelector('button[name="dash"]'),
                            numberRelic = relicHtml.querySelector('span.number-relics');

                        buttonPlus.onclick = () => this.plusRelic(relic.slug, buttonPlus, buttonDash, numberRelic);
                        buttonDash.onclick = () => this.dashRelic(relic.slug, buttonDash, numberRelic);

                        typeHtml.append(relicHtml)
                        this.listRelics.append(typeHtml)
                    })
                ;
            })
        ;
    }

    /**
     * @param {string} slug
     * @param {HTMLButtonElement} buttonPlus
     * @param {HTMLButtonElement} buttonDash
     * @param {HTMLSpanElement} numberRelic
     */
    plusRelic(slug, buttonPlus, buttonDash, numberRelic) {
        numberRelic.textContent = this.distributionService.plusRelic(slug, Number.parseInt(numberRelic.textContent)).toString();
        buttonDash.disabled = false;
    }

    /**
     @param {string} slug
     * @param {HTMLButtonElement} buttonDash
     * @param {HTMLSpanElement} numberRelic
     */
    dashRelic(slug, buttonDash, numberRelic) {
        let number = this.distributionService.dashRelic(slug, Number.parseInt(numberRelic.textContent));
        numberRelic.textContent = number.toString();
        buttonDash.disabled = number === 0;
    }

    reset() {
        this
            .listRelics
            .querySelectorAll('span.number-relics')
            .forEach(numberRelics => numberRelics.textContent = "0")
        ;
        this.distributionService.calculTotalAP();
    }
}