import {MemberModel} from "../model/member.model.js";

export class MembersService {
    /** @type {DistributionService} */
    distributionService;
    /** @type {HTMLButtonElement} */
    buttonAdd = document.getElementById('add-member');
    /** @type {HTMLInputElement} */
    inputPseudo = document.getElementById('namePlayer');
    /** @type {HTMLUListElement} */
    membersView = document.querySelector('ul.list-group.list-group-numbered');
    parser = new DOMParser();

    constructor(distributionService) {
        this.distributionService = distributionService;
        this.buttonAdd.onclick = () => this.addMember();
        this.inputPseudo.oninput = () => this.checkInputPseudo();
    }

    addMember() {
        this.buttonAdd.disabled = true;

        if (this.inputPseudo.value.trim().length < 3 || this.distributionService.members.filter(member => member.pseudo === this.inputPseudo.value).length > 0) {
            return;
        }

        this.distributionService.member = new MemberModel(this.inputPseudo.value);
        this.inputPseudo.value = null;

        this.renderView();
    }

    checkInputPseudo() {
        this.inputPseudo.value = this.inputPseudo.value.trimStart();
        this.buttonAdd.disabled = this.inputPseudo.value.trim().length < 3 || this.distributionService.members.filter(member => member.pseudo === this.inputPseudo.value).length > 0;
    }

    renderView() {
        Array
            .from(this.membersView.children)
            .forEach(child => child.remove())
        ;

        this
            .distributionService
            .members
            .forEach(member => {
                let html = `
                    <li class="list-group-item d-flex justify-content-between align-items-start">
                        <div class="ms-2 me-auto">
                            <div class="fw-bold">${member.pseudo}</div>
                        </div>
                        <span class="badge bg-primary rounded-pill">${new Intl.NumberFormat('fr-FR').format(this.distributionService.pointsByRelic(...member.relics))}</span>
                    </li>
                `;

                this
                    .membersView
                    .append(
                        this
                            .parser
                            .parseFromString(html, 'text/html')
                            .body
                            .firstElementChild
                    )
                ;
            })
        ;
    }
}