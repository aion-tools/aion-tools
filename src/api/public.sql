drop database if exists tools_aion;
create database tools_aion;
\c tools_aion

create table if not exists relic_types (
    "uuid" uuid primary key default gen_random_uuid(),
    slug varchar(50) unique not null
);

create table if not exists relics (
    "uuid" uuid primary key default gen_random_uuid(),
    slug varchar(50) unique not null,
    fk_uuid_type uuid not null references relic_types(uuid)
);

create table if not exists rates (
    "uuid" uuid primary key default gen_random_uuid(),
    slug varchar(50) unique not null
);

create table if not exists points (
    quantity int not null check (quantity >= 0),
    fk_uuid_relic uuid not null references relics(uuid),
    fk_uuid_rate uuid not null references rates(uuid)
);

create view full_relics as select
   relics.slug as name,
   relic_types.slug as type,
   points.quantity,
   rates.slug as rate
from relics
join relic_types on relic_types.uuid = relics.fk_uuid_type
join points on points.fk_uuid_relic = relics.uuid
join rates on rates.uuid = points.fk_uuid_rate;

insert into relic_types (slug)
values
    ('icon'),
    ('seal'),
    ('chalice'),
    ('crown')
;

insert into relics (slug, fk_uuid_type)
values
    ('icon.lesser_ancient', (select uuid from relic_types where slug = 'icon')),
    ('icon.ancient', (select uuid from relic_types where slug = 'icon')),
    ('icon.greater_ancient', (select uuid from relic_types where slug = 'icon')),
    ('icon.major_ancient', (select uuid from relic_types where slug = 'icon')),
    ('seal.lesser_ancient', (select uuid from relic_types where slug = 'seal')),
    ('seal.ancient', (select uuid from relic_types where slug = 'seal')),
    ('seal.greater_ancient', (select uuid from relic_types where slug = 'seal')),
    ('seal.major_ancient', (select uuid from relic_types where slug = 'seal')),
    ('chalice.lesser_ancient', (select uuid from relic_types where slug = 'chalice')),
    ('chalice.ancient', (select uuid from relic_types where slug = 'chalice')),
    ('chalice.greater_ancient', (select uuid from relic_types where slug = 'chalice')),
    ('chalice.major_ancient', (select uuid from relic_types where slug = 'chalice')),
    ('crown.lesser_ancient', (select uuid from relic_types where slug = 'crown')),
    ('crown.ancient', (select uuid from relic_types where slug = 'crown')),
    ('crown.greater_ancient', (select uuid from relic_types where slug = 'crown')),
    ('crown.major_ancient', (select uuid from relic_types where slug = 'crown'))
;

insert into rates (slug)
values
    ('normal'),
    ('apheta beluslan')
;

insert into points (quantity, fk_uuid_relic , fk_uuid_rate)
values
    (300, (select uuid from relics where slug = 'icon.lesser_ancient'), (select uuid from rates where slug = 'normal')),
    (450, (select uuid from relics where slug = 'icon.lesser_ancient'), (select uuid from rates where slug = 'apheta beluslan')),
    (600, (select uuid from relics where slug = 'icon.ancient'), (select uuid from rates where slug = 'normal')),
    (900, (select uuid from relics where slug = 'icon.ancient'), (select uuid from rates where slug = 'apheta beluslan')),
    (900, (select uuid from relics where slug = 'icon.greater_ancient'), (select uuid from rates where slug = 'normal')),
    (1350, (select uuid from relics where slug = 'icon.greater_ancient'), (select uuid from rates where slug = 'apheta beluslan')),
    (1200, (select uuid from relics where slug = 'icon.major_ancient'), (select uuid from rates where slug = 'normal')),
    (1800, (select uuid from relics where slug = 'icon.major_ancient'), (select uuid from rates where slug = 'apheta beluslan')),
    (600, (select uuid from relics where slug = 'seal.lesser_ancient'), (select uuid from rates where slug = 'normal')),
    (900, (select uuid from relics where slug = 'seal.lesser_ancient'), (select uuid from rates where slug = 'apheta beluslan')),
    (1200, (select uuid from relics where slug = 'seal.ancient'), (select uuid from rates where slug = 'normal')),
    (1800, (select uuid from relics where slug = 'seal.ancient'), (select uuid from rates where slug = 'apheta beluslan')),
    (1800, (select uuid from relics where slug = 'seal.greater_ancient'), (select uuid from rates where slug = 'normal')),
    (2700, (select uuid from relics where slug = 'seal.greater_ancient'), (select uuid from rates where slug = 'apheta beluslan')),
    (2400, (select uuid from relics where slug = 'seal.major_ancient'), (select uuid from rates where slug = 'normal')),
    (3600, (select uuid from relics where slug = 'seal.major_ancient'), (select uuid from rates where slug = 'apheta beluslan')),
    (800, (select uuid from relics where slug = 'chalice.lesser_ancient'), (select uuid from rates where slug = 'normal')),
    (1200, (select uuid from relics where slug = 'chalice.lesser_ancient'), (select uuid from rates where slug = 'apheta beluslan')),
    (1600, (select uuid from relics where slug = 'chalice.ancient'), (select uuid from rates where slug = 'normal')),
    (2400, (select uuid from relics where slug = 'chalice.ancient'), (select uuid from rates where slug = 'apheta beluslan')),
    (2400, (select uuid from relics where slug = 'chalice.greater_ancient'), (select uuid from rates where slug = 'normal')),
    (3600, (select uuid from relics where slug = 'chalice.greater_ancient'), (select uuid from rates where slug = 'apheta beluslan')),
    (3200, (select uuid from relics where slug = 'chalice.major_ancient'), (select uuid from rates where slug = 'normal')),
    (4800, (select uuid from relics where slug = 'chalice.major_ancient'), (select uuid from rates where slug = 'apheta beluslan')),
    (1600, (select uuid from relics where slug = 'crown.lesser_ancient'), (select uuid from rates where slug = 'normal')),
    (2400, (select uuid from relics where slug = 'crown.lesser_ancient'), (select uuid from rates where slug = 'apheta beluslan')),
    (3200, (select uuid from relics where slug = 'crown.ancient'), (select uuid from rates where slug = 'normal')),
    (4800, (select uuid from relics where slug = 'crown.ancient'), (select uuid from rates where slug = 'apheta beluslan')),
    (4800, (select uuid from relics where slug = 'crown.greater_ancient'), (select uuid from rates where slug = 'normal')),
    (7200, (select uuid from relics where slug = 'crown.greater_ancient'), (select uuid from rates where slug = 'apheta beluslan')),
    (6400, (select uuid from relics where slug = 'crown.major_ancient'), (select uuid from rates where slug = 'normal')),
    (9600, (select uuid from relics where slug = 'crown.major_ancient'), (select uuid from rates where slug = 'apheta beluslan'))
;