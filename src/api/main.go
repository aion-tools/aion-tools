package api

import (
	"aion-tools/src/middleware"
	"database/sql"
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"log"
	"net/http"
	"os"
)

type Rates struct {
	Slug string `json:"slug"`
}

type Relics struct {
	Name     string `json:"slug"`
	Type     string `json:"type"`
	Quantity int    `json:"quantity"`
	Rate     Rates  `json:"rate"`
}

var database *sql.DB

func Router(router *mux.Router) {
	err := godotenv.Load()
	if err != nil {
		log.Fatalf("Error loading .env file: %v", err)
	}

	db, err := sql.Open("postgres", os.Getenv("DB_URL"))
	if err != nil {
		log.Fatalf("Error connection database: %v", err)
	}
	database = db

	// todo: for the route "/", create a web documentation
	router.HandleFunc("/rates", middleware.Chain(ratesAll, middleware.Logging())).Methods("GET")
	router.HandleFunc("/relics", middleware.Chain(relicsAll, middleware.Logging())).Methods("GET")
	router.HandleFunc("/relics/{rate}", middleware.Chain(relicsAllBySlug, middleware.Logging())).Methods("GET")
}

func ratesAll(w http.ResponseWriter, r *http.Request) {
	rows, err := database.Query("select slug from rates")
	if err != nil {
		log.Fatalf("Error query database: %v", err)
	}
	defer rows.Close()

	var rates []Rates

	for rows.Next() {
		var rate Rates
		rows.Scan(&rate.Slug)
		rates = append(rates, rate)
	}

	json.NewEncoder(w).Encode(rates)
}

func relicsAll(w http.ResponseWriter, r *http.Request) {
	rows, err := database.Query("select name, type, quantity, rate from full_relics")
	if err != nil {
		log.Fatalf("Error query database: %v", err)
	}
	defer rows.Close()

	var relics []Relics

	for rows.Next() {
		var (
			relic Relics
			rate  Rates
		)

		rows.Scan(&relic.Name, &relic.Type, &relic.Quantity, &rate.Slug)
		relic.Rate = rate
		relics = append(relics, relic)
	}

	json.NewEncoder(w).Encode(relics)
}

func relicsAllBySlug(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	rows, err := database.Query("select name, type, quantity, rate from full_relics where rate = $1", vars["rate"])
	if err != nil {
		log.Fatalf("Error query database: %v", err)
	}
	defer rows.Close()

	var relics []Relics

	for rows.Next() {
		var (
			relic Relics
			rate  Rates
		)

		rows.Scan(&relic.Name, &relic.Type, &relic.Quantity, &rate.Slug)
		relic.Rate = rate
		relics = append(relics, relic)
	}

	json.NewEncoder(w).Encode(relics)
}
