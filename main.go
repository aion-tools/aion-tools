package main

import (
	"aion-tools/src/api"
	"aion-tools/src/middleware"
	"github.com/gorilla/mux"
	"html/template"
	"log"
	"net/http"
	"os"
)

func index(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles("template/base.html", "template/index.html"))
	tmpl.Execute(w, nil)
}

func main() {
	r := mux.NewRouter()

	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("public"))))

	r.HandleFunc("/", middleware.Chain(index, middleware.Logging()))
	api.Router(r.PathPrefix("/api").Subrouter())

	log.Fatal(http.ListenAndServe(":"+os.Getenv("APP_PORT"), r))
}
